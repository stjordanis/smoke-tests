package main

import (
	"flag"
	"fmt"

	"github.com/binance-chain/go-sdk/keys"
)

func main() {
	mnemonic := flag.String("m", "", "The mnemonic phrase of the wallet.")
	flag.Parse()

	keyManager, _ := keys.NewMnemonicKeyManager(*mnemonic)
	privateKey, _ := keyManager.ExportAsPrivateKey()

	fmt.Printf("Your private key is: %s\n", privateKey)
}
