package main

import (
	"flag"
	"fmt"

	sdk "github.com/binance-chain/go-sdk/client"
	btypes "github.com/binance-chain/go-sdk/common/types"
	"github.com/binance-chain/go-sdk/keys"
)

// main : Generate our Admin, Staker, User and Pool details.
func main() {
	stakerCount := flag.Int("s", 1, "The number of stakers to create.")
	flag.Parse()

	// Admin
	adminK := bnbClientKey()
	privKey, _ := adminK.ExportAsPrivateKey()
	fmt.Printf("Admin: \t%v, %v\n", adminK.GetAddr().String(), privKey)

	// Staker
	for i := 1; i <= *stakerCount; i++ {
		stakerK := bnbClientKey()
		privKey, _ = stakerK.ExportAsPrivateKey()
		fmt.Printf("Staker%v: %v, %v\n", i, stakerK.GetAddr().String(), privKey)
	}

	// User
	userK := bnbClientKey()
	privKey, _ = userK.ExportAsPrivateKey()
	fmt.Printf("User: \t%v, %v\n", userK.GetAddr().String(), privKey)

	// Pool
	poolK := bnbClientKey()
	privKey, _ = poolK.ExportAsPrivateKey()
	fmt.Printf("Pool: \t%v, %v\n", poolK.GetAddr().String(), privKey)
}

// bnbClientKey : Generate a new private key (and tbnb address).
func bnbClientKey() keys.KeyManager {
	keyManager, _ := keys.NewKeyManager()
	sdk.NewDexClient("testnet-dex.binance.org", btypes.TestNetwork, keyManager)

	return keyManager
}
