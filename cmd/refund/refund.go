package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	sdk "github.com/binance-chain/go-sdk/client"
	"github.com/binance-chain/go-sdk/client/transaction"
	btypes "github.com/binance-chain/go-sdk/common/types"
	"github.com/binance-chain/go-sdk/keys"
	"github.com/binance-chain/go-sdk/types/msg"
)

// Pools : Statechain pool struct.
type Pools []struct {
	BalanceRune  string    `json:"balance_rune"`
	BalanceToken string    `json:"balance_token"`
	Ticker       string    `json:"ticker"`
	PoolUnits    string    `json:"pool_units"`
	PoolAddress  string    `json:"pool_address"`
	Status       string    `json:"status"`
	ExpiryUtc    time.Time `json:"expiry_utc"`
}

// main : Get our pools and issue a refund to all stakers.
func main() {
	adminKey := flag.String("a", "", "The admin private key.")
	poolEndpoint := flag.String("p", "", "The Statechain pool endpoint to use.")
	flag.Parse()

	keyManager, _ := keys.NewPrivateKeyManager(*adminKey)
	client, _ := sdk.NewDexClient("testnet-dex.binance.org", btypes.TestNetwork, keyManager)

	pools, _ := getPools(*poolEndpoint)
	for _, pool := range pools {
		coins := btypes.Coins{btypes.Coin{Denom: pool.Ticker, Amount: 1}}

		var payload []msg.Transfer
		payload = append(payload, msg.Transfer{btypes.AccAddress(pool.PoolAddress), coins})

		send, err := client.SendToken(payload, true, transaction.WithMemo(fmt.Sprintf("ADMIN:END:%s", pool.Ticker)))
		if err != nil {
			log.Printf("Error: %v\n", err)
		}

		log.Printf("Response from Binance: %v\n", send)
	}
}

// getPools : Get the current list of pools on the Statechain.
func getPools(poolEndpoint string) (Pools, error) {
	var pools Pools

	resp, err := http.Get(poolEndpoint)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	data, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(data, &pools)
	if err != nil {
		return nil, err
	}

	return pools, nil
}
