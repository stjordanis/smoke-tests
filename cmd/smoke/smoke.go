package main

import (
	"flag"

	"gitlab.com/thorchain/bepswap/smoke-tests/x/smoke"
	"gitlab.com/thorchain/bepswap/smoke-tests/x/smoke/types"
)

func main() {
	masterKey := flag.String("m", "", "The master private key.")
	poolAddr := flag.String("p", "", "The pool address.")
	config := flag.String("c", types.DefaultConfig, "Path to the config file.")
	keysFile := flag.String("f", "", "Path to file to export actors private keys.")
	flag.Parse()

	s := smoke.NewSmoke(*masterKey, *poolAddr, *config, *keysFile)
	s.Run()
}
