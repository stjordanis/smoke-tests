package types

const (
	ChainId    = "Binance-Chain-Nile"
	TestNet    = "testnet-dex.binance.org"
	ApiUri     = "api/v1/broadcast"
	Multiplier = 100000000
)
