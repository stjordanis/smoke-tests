package hoover

import (
	"log"

	sdk "github.com/binance-chain/go-sdk/client"
	btypes "github.com/binance-chain/go-sdk/common/types"
	"github.com/binance-chain/go-sdk/keys"
	"github.com/binance-chain/go-sdk/types/msg"

	"gitlab.com/thorchain/bepswap/smoke-tests/x/binance"
	"gitlab.com/thorchain/bepswap/smoke-tests/x/binance/types"
)

// Hoover : our main Hoover type.
type Hoover struct {
	Binance    binance.Binance
	KeyManager keys.KeyManager
	Client     sdk.DexClient
	KeyList    []string
}

// NewHoover : Create a new instance of Hoover.
func NewHoover(masterPrivKey string, keyList []string) Hoover {
	keyManager, _ := keys.NewPrivateKeyManager(masterPrivKey)
	client, _ := sdk.NewDexClient(types.TestNet, btypes.TestNetwork, keyManager)

	return Hoover{
		Binance:    binance.NewBinance(true),
		KeyManager: keyManager,
		Client:     client,
		KeyList:    keyList,
	}
}

// EmptyWallets : Empty and transfer all assets out of the wallet.
func (h Hoover) EmptyWallets() {
	for _, key := range h.KeyList {
		keyManager, _ := keys.NewPrivateKeyManager(key)
		client, _ := sdk.NewDexClient(types.TestNet, btypes.TestNetwork, keyManager)

		balances := h.Balances(keyManager.GetAddr())
		for _, token := range balances {
			free := float64(token.Free)
			amt := int64(free)
			if token.Symbol == "BNB" {
				amt = amt - 375000
			}

			coins := btypes.Coins{btypes.Coin{Denom: token.Symbol, Amount: amt}}

			var payload []msg.Transfer
			payload = append(payload, msg.Transfer{h.KeyManager.GetAddr(), coins})

			h.SendTxn(client, keyManager, payload, "")
		}
	}
}

// Balances : Get the account balances of a given wallet.
func (h Hoover) Balances(address btypes.AccAddress) []btypes.TokenBalance {
	acct, err := h.Client.GetAccount(address.String())
	if err != nil {
		log.Fatal(err)
	}

	return acct.Balances
}

// SendTxn : Send our transaction to Binance
func (h Hoover) SendTxn(client sdk.DexClient, key keys.KeyManager, payload []msg.Transfer, memo string) {
	h.Binance.SendTxn(client, key, payload, memo)
}
