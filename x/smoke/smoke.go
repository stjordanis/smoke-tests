package smoke

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	sdk "github.com/binance-chain/go-sdk/client"
	stypes "github.com/binance-chain/go-sdk/common/types"
	"github.com/binance-chain/go-sdk/keys"
	"github.com/binance-chain/go-sdk/types/msg"

	"gitlab.com/thorchain/bepswap/smoke-tests/x/binance"
	btypes "gitlab.com/thorchain/bepswap/smoke-tests/x/binance/types"
	"gitlab.com/thorchain/bepswap/smoke-tests/x/smoke/types"
)

// Smoke : wallets.
type Smoke struct {
	delay     time.Duration
	MasterKey string
	PoolAddr  string
	KeysFile  string
	Binance   binance.Binance
	Tests     types.Tests
}

// NewSmoke : create a new Smoke instance
func NewSmoke(masterKey, poolAddr, config, keysFile string) Smoke {
	cfg, err := ioutil.ReadFile(config)
	if err != nil {
		log.Fatal(err)
	}

	var tests types.Tests
	json.Unmarshal(cfg, &tests)

	return Smoke{
		delay:     2 * time.Second,
		MasterKey: masterKey,
		PoolAddr:  poolAddr,
		Binance:   binance.NewBinance(true),
		Tests:     tests,
		KeysFile:  keysFile,
	}
}

// Setup : Generate/setup our accounts.
func (s *Smoke) Setup() {
	// Master
	mKey, _ := keys.NewPrivateKeyManager(s.MasterKey)
	mClient, _ := sdk.NewDexClient(btypes.TestNet, stypes.TestNetwork, mKey)

	s.Tests.Actors.Master.Key = mKey
	s.Tests.Actors.Master.Client = mClient

	// Admin
	aClient, aKey := s.ClientKey("admin")
	s.Tests.Actors.Admin.Key = aKey
	s.Tests.Actors.Admin.Client = aClient

	// Stakers
	for i := 1; i <= s.Tests.StakerCount; i++ {
		sClient, sKey := s.ClientKey(fmt.Sprintf("staker_%d", i))
		s.Tests.Actors.Stakers = append(s.Tests.Actors.Stakers, types.Keys{Key: sKey, Client: sClient})
	}

	// User
	uClient, uKey := s.ClientKey("user")
	s.Tests.Actors.User.Key = uKey
	s.Tests.Actors.User.Client = uClient

	s.Summary()
	s.ExportKeysToFile()
}

// ClientKey : instantiate Client and Keys Binance SDK objects.
func (s *Smoke) ClientKey(name string) (client sdk.DexClient, keyManager keys.KeyManager) {
	filename := fmt.Sprintf("/data/.%s", name)
	if _, err := os.Stat(filename); err == nil {
		privKey, err := ioutil.ReadFile(filename)
		if err != nil {
			log.Fatal(err)
		}
		keyManager, _ = keys.NewPrivateKeyManager(string(privKey))
	} else {
		keyManager, _ = keys.NewKeyManager()
		privKey, _ := keyManager.ExportAsPrivateKey()
		err := ioutil.WriteFile(filename, []byte(privKey), 0600)
		if err != nil {
			log.Fatal(err)
		}
	}
	client, _ = sdk.NewDexClient("testnet-dex.binance.org", stypes.TestNetwork, keyManager)
	return client, keyManager
}

// ExportKeysToFile : Export private keys to file separated by comma to reuse later
func (s *Smoke) ExportKeysToFile() {
	if s.KeysFile == "" {
		return
	}
	f, err := os.OpenFile(s.KeysFile, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		log.Fatal(err)
	}

	defer f.Close()

	privKey, _ := s.Tests.Actors.Admin.Key.ExportAsPrivateKey()
	if _, err = f.WriteString(privKey); err != nil {
		log.Fatal(err)
	}
	privKey, _ = s.Tests.Actors.User.Key.ExportAsPrivateKey()
	if _, err = f.WriteString(fmt.Sprintf(",%s", privKey)); err != nil {
		log.Fatal(err)
	}
	for _, staker := range s.Tests.Actors.Stakers {
		privKey, _ = staker.Key.ExportAsPrivateKey()
		if _, err = f.WriteString(fmt.Sprintf(",%s", privKey)); err != nil {
			log.Fatal(err)
		}
	}
}

// Summary : Private Keys
func (s *Smoke) Summary() {
	privKey, _ := s.Tests.Actors.Admin.Key.ExportAsPrivateKey()
	fmt.Printf("Admin: %v - %v\n", s.Tests.Actors.Admin.Key.GetAddr(), privKey)

	privKey, _ = s.Tests.Actors.User.Key.ExportAsPrivateKey()
	fmt.Printf("User: %v - %v\n", s.Tests.Actors.User.Key.GetAddr(), privKey)

	for idx, staker := range s.Tests.Actors.Stakers {
		privKey, _ = staker.Key.ExportAsPrivateKey()
		fmt.Printf("Staker %v: %v - %v\n", idx, staker.Key.GetAddr(), privKey)
	}
}

// Run : Where there's smoke, there's fire!
func (s *Smoke) Run() {
	s.Setup()

	for _, rule := range s.Tests.Rules {
		var payload []msg.Transfer
		var coins []stypes.Coin

		for _, coin := range rule.Coins {
			coins = append(coins, stypes.Coin{Denom: coin.Denom, Amount: int64(coin.Amount * 100000000)})
		}

		for _, to := range rule.To {
			toAddr := s.ToAddr(to)
			msg := msg.Transfer{
				ToAddr: toAddr,
				Coins:  coins,
			}
			payload = append(payload, msg)
		}

		client, key := s.FromClientKey(rule.From)
		s.SendTxn(client, key, payload, rule.Memo)

		if rule.Check.Target == "to" {
			for _, to := range rule.To {
				toAddr := s.ToAddr(to)
				s.CheckWallet(toAddr, rule.Check, rule.Description)
			}
		} else {
			s.CheckWallet(key.GetAddr(), rule.Check, rule.Description)
		}

		if rule.Check.Pool.Units != 0 {
			s.CheckPoolUnits(rule.Check.Pool.Units, rule.Description)
		}
	}
}

// FromClientKey : Client and key based on the rule "from".
func (s *Smoke) FromClientKey(from string) (sdk.DexClient, keys.KeyManager) {
	switch from {
	case "master":
		return s.Tests.Actors.Master.Client, s.Tests.Actors.Master.Key
	case "admin":
		return s.Tests.Actors.Admin.Client, s.Tests.Actors.Admin.Key
	case "user":
		return s.Tests.Actors.User.Client, s.Tests.Actors.User.Key
	default:
		stakerIdx := strings.Split(from, "_")[1]
		i, _ := strconv.Atoi(stakerIdx)
		staker := s.Tests.Actors.Stakers[i-1]
		return staker.Client, staker.Key
	}
}

// ToAddr : To address
func (s *Smoke) ToAddr(to string) stypes.AccAddress {
	switch to {
	case "admin":
		return s.Tests.Actors.Admin.Key.GetAddr()
	case "user":
		return s.Tests.Actors.User.Key.GetAddr()
	case "pool":
		addr, err := stypes.AccAddressFromBech32(s.PoolAddr)
		if err != nil {
			log.Fatal(err)
		}
		return addr
	default:
		stakerIdx := strings.Split(to, "_")[1]
		i, _ := strconv.Atoi(stakerIdx)
		return s.Tests.Actors.Stakers[i-1].Key.GetAddr()
	}
}

// Balances : Get the account balances of a given wallet.
func (s *Smoke) Balances(address stypes.AccAddress) []stypes.TokenBalance {
	acct, err := s.Tests.Actors.Master.Client.GetAccount(address.String())
	if err != nil {
		log.Fatal(err)
	}

	return acct.Balances
}

// SendTxn : Send the transaction to Binance.
func (s *Smoke) SendTxn(client sdk.DexClient, key keys.KeyManager, payload []msg.Transfer, memo string) {
	s.Binance.SendTxn(client, key, payload, memo)
}

// CheckWallet : Check the balances
func (s *Smoke) CheckWallet(address stypes.AccAddress, check types.Check, memo string) {
	time.Sleep(s.delay)
	balances := s.Balances(address)

	for _, coins := range check.Wallet {
		for _, balance := range balances {
			if coins.Denom == balance.Symbol {
				amount := coins.Amount * 100000000
				free := float64(balance.Free)

				if amount != free {
					log.Printf("%v: %v - FAIL: Amounts do not match - %f versus %f",
						memo,
						address.String(),
						amount,
						free,
					)
				} else {
					log.Printf("%v: %v - PASS", memo, address.String())
				}
			}
		}
	}
}

// GetPools : Get our pools.
func (s *Smoke) GetPools() types.Pools {
	var pools types.Pools

	resp, err := http.Get(types.StatechainURL)
	if err != nil {
		log.Printf("%v\n", err)
	}
	defer resp.Body.Close()

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Printf("%v\n", err)
	}

	json.Unmarshal(data, &pools)

	return pools
}

// CheckPoolUnits : Check pool units
func (s *Smoke) CheckPoolUnits(units float64, memo string) {
	pools := s.GetPools()
	for _, pool := range pools {
		if pool.Symbol == types.PoolSymbol {
			poolUnits, _ := strconv.ParseFloat(pool.PoolUnits, 64)
			if units != poolUnits {
				log.Printf("%v: FAIL: Pool Units do not match - %f versus %f",
					memo,
					units,
					poolUnits,
				)
			} else {
				log.Printf("%v: PASS", memo)
			}
		}
	}
}
