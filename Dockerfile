FROM golang:1.13 AS build

ENV GOBIN=/go/bin
ENV GOPATH=/go
ENV CGO_ENABLED=0
ENV GOOS=linux

WORKDIR /app
COPY go.mod go.sum ./
RUN go mod download

COPY . .

RUN go build -a -installsuffix cgo -o $GOBIN/hoover ./cmd/hoover
RUN go build -a -installsuffix cgo -o $GOBIN/smoke ./cmd/smoke
RUN go build -a -installsuffix cgo -o $GOBIN/seed ./cmd/seed
RUN go build -a -installsuffix cgo -o $GOBIN/key ./cmd/key

#
# Main
#
FROM alpine

COPY ./cmd/smoke/*.json /smoke/
COPY ./scripts /scripts

# Copy the compiled binaires over.
COPY --from=build /go/bin/hoover /usr/bin/
COPY --from=build /go/bin/smoke /usr/bin/
COPY --from=build /go/bin/seed /usr/bin/
COPY --from=build /go/bin/key /usr/bin/
